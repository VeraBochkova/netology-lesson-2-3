<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Form</title>
</head>
<body>
    <p>Выберите .json файл с тестом для загрузки на сервер:</p>
    <form action="admin.php" method="POST" enctype="multipart/form-data">
        <div><input type="file" name="test"></div>
        <br>
        <div><input type="submit" value="Отправить"></div>
        <br>
    </form>
</body>
</html>

<?php
#echo "<pre>";
#var_dump($_FILES);

$upload_dir = 'Tests/';
if (!empty($_FILES) && array_key_exists('test', $_FILES)) {
    if (move_uploaded_file($_FILES['test']['tmp_name'], $upload_dir . $_FILES['test']['name'])) {
        header('Location: list.php');
        exit;
    }
    else {
        echo ' Ошибка загрузки файла. ';
    }
}
?>